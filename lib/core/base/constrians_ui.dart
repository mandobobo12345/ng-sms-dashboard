import 'package:flutter/material.dart';

import '../components/custom_appbar.dart';

class ConstraintsUi extends StatefulWidget {
  final Widget mobileWidget;
  final Widget tabletWidget;
  final Widget windowsWidget;
  const ConstraintsUi(
      {super.key,
      required this.mobileWidget,
      required this.tabletWidget,
      required this.windowsWidget});

  @override
  State<ConstraintsUi> createState() => _ConstraintsUiState();
}

class _ConstraintsUiState extends State<ConstraintsUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        showDrawerIcon: MediaQuery.sizeOf(context).width < 850 ? false :true,
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth < 850) {
            return widget.mobileWidget;
          } else if (constraints.maxWidth < 1100 &&
              constraints.maxWidth >= 850) {
            return widget.tabletWidget;
          } else {
            return widget.windowsWidget;
          }
        },
      ),
    );
  }
}
