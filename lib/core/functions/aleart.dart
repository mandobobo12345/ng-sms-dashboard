import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../components/button_component.dart';
import '../components/text_component.dart';
import '../components/text_field_component.dart';

void alertDialog({
  required BuildContext context,
  required String? contentText,
}) {
  showDialog(
      context: context,
      builder: (context) {
        TextEditingController textController = TextEditingController();
        textController.text = contentText.toString();
        return AlertDialog(
          backgroundColor: Colors.white,
          title: const TextComponent(
            text: 'Content Message',
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          scrollable: true,
          elevation: 1,
          content: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextFieldComponent(
                  labelText: "",
                  enabled: false,
                  controller: textController,
                  maxLines: textController.text.length,
                  minLines: textController.text.length.sign,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width * .02,
                ),
                ButtonComponent(
                  width: MediaQuery.of(context).size.width * .05,
                  padding: 0,
                  onPressed: () {
                    final copy = ClipboardData(text: textController.text);
                    Clipboard.setData(copy);
                  },
                  backGroundColor: Colors.indigo.shade900,
                  text: "Copy",
                  textColor: Colors.white,
                )
              ],
            ),
          ),
        );
      });
}
