import 'package:flutter/material.dart';

 import '../base/responsive.dart';

class IconDrawer extends StatelessWidget {
  final GlobalKey<ScaffoldState> keyDrawer;
  const IconDrawer({required this.keyDrawer, super.key});

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;

    return Padding(
        padding: EdgeInsets.only(top: media.height * .05),
        child: Row(children: [
          if (!Responsive.isDesktop(context))
            IconButton(
              onPressed: () {
                if (keyDrawer.currentState!.isDrawerOpen == false) {
                  keyDrawer.currentState!.openDrawer();
                } else {
                  keyDrawer.currentState!.closeDrawer();
                }
              },
              icon: Icon(
                Icons.menu,
                color: Theme.of(context).primaryColor,
              ),
            ),
        ]));
  }
}
