// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class TextFieldComponent extends StatelessWidget {
  final TextEditingController? controller;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final double? height;
  final double? width;
  final dynamic fontSize;

  final dynamic cursorHeight;
  final dynamic showCursor;
  final int? length;
  final int? maxLines;
  final int? minLines;
  final String? labelText;
  final String? hinText;
  final bool? fill;
  final Color? fillColor;
  final Color? borderColor;
  final Color? enabledBorderColor;
  final Color? colorSuffixIcon;
  final dynamic enabledBorderCurve;
  final Color? labelColor;
  final InputBorder? styleBorder;
  final String? Function(String?)? valid;
  final String? Function(String?)? onChange;

  final void Function()? onTap;
  final bool? enabled;
  final TextInputType? textInputType;
  final dynamic curveBorder;
  final bool? obscureText;

  void Function()? onPressedSuffixIcon;

  TextFieldComponent(
      {Key? key,
      required this.controller,
      this.prefixIcon,
      this.colorSuffixIcon,
      this.suffixIcon,
      this.fontSize,
      this.onPressedSuffixIcon,
      this.enabledBorderCurve,
      this.textInputType,
      this.curveBorder,
      this.labelColor,
      this.onChange,
      this.onTap,
      this.maxLines,
      this.hinText,
      this.minLines,
      this.fill,
      this.fillColor,
      this.cursorHeight,
      this.valid,
      this.labelText,
      this.showCursor,
      this.enabled,
      this.length,
      this.height,
      this.styleBorder,
      this.enabledBorderColor,
      this.width,
      this.obscureText,
      this.borderColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Theme(
        data: Theme.of(context).copyWith(
            colorScheme:
                ColorScheme.light(primary: enabledBorderColor ?? Colors.white)),
        child: TextFormField(
          style: const TextStyle(color: Colors.black),
          obscureText:
              obscureText == null || obscureText == false ? false : true,
          keyboardType: textInputType,
          onChanged: onChange,
          onTap: onTap,
          enabled: enabled,
          validator: valid,
          controller: controller,
          minLines: minLines,
          maxLines: maxLines,
          maxLength: length,
          showCursor: showCursor,
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            isDense: true,
            suffixIcon: suffixIcon == null
                ? null
                : IconButton(
                    onPressed: onPressedSuffixIcon,
                    icon: Icon(
                      suffixIcon,
                      color: colorSuffixIcon,
                    )),
            filled: true,
            fillColor: fillColor ?? Colors.grey.shade300,
            labelText: labelText!,
            labelStyle: TextStyle(
                color: labelColor ?? Colors.black54,
                fontWeight: FontWeight.bold),
            hintText: hinText,
            prefixIcon: prefixIcon == null ? null : Icon(prefixIcon),
            enabledBorder: styleBorder ??
                OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(curveBorder ?? 13),
                ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(enabledBorderCurve ?? 9),
            ),
          ),
        ),
      ),
    );
  }
}
