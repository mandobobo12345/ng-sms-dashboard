import 'package:flutter/material.dart';

class OnHoverComponent extends StatefulWidget {
  final Widget? child;
  final Widget Function(bool isHoverd)? builder;

  const OnHoverComponent({this.child, this.builder, super.key});

  @override
  State<OnHoverComponent> createState() => _OnHoverComponentState();
}

class _OnHoverComponentState extends State<OnHoverComponent> {
  bool isHoverd = false;

  @override
  Widget build(BuildContext context) {
    final animationHover = Matrix4.identity()..translate(0, 2, 0);
    final transform = isHoverd ? animationHover : Matrix4.identity();

    void enterHover(bool isHoverdMouse) => setState(() {
          isHoverd = isHoverdMouse;
        });

    return MouseRegion(
      onEnter: (event) => enterHover(true),
      onExit: (event) => enterHover(false),
      child: Center(
        child: AnimatedContainer(
            duration: const Duration(milliseconds: 200),
            transform: transform,
            child: widget.child ?? widget.builder!(isHoverd)),
      ),
    );
  }
}
