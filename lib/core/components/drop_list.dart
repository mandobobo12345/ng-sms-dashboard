// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class DropListWidget extends StatefulWidget {
  final List<String>? list;
  dynamic value;
  final double? height;
  final double? width;
  final Color? color;
  final Color? textColor;
  final double? borderRadius;
  final Color? colorBorder;
  final Color? iconColor;
  String? Function(String?)? onChange;

  DropListWidget(
      {this.width,
      this.height,
      this.colorBorder,
      this.onChange,
      this.color,
      this.textColor,
      this.iconColor,
      this.borderRadius,
      required this.list,
      required this.value,
      Key? key})
      : super(key: key);

  @override
  State<DropListWidget> createState() => _DropList();
}

class _DropList extends State<DropListWidget> {
  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;
    return Container(
      width: widget.width,
      height: widget.height,
      padding: EdgeInsets.symmetric(horizontal: media.width * .03),
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
        color: widget.color ?? Colors.white,
        border: Border.all(color: widget.colorBorder ?? Colors.black87),
        borderRadius: BorderRadius.circular(widget.borderRadius ?? 10),
      ),
      child: DropdownButton(
        underline: const SizedBox(),
        alignment: Alignment.topLeft,
        iconEnabledColor: widget.iconColor ?? Colors.deepPurple,
        iconSize: media.height * .03,
        itemHeight: 60.0,
        isExpanded: true,
        dropdownColor:
            widget.textColor == Colors.white ? Colors.grey : Colors.white,
        style: TextStyle(
            color: widget.textColor ?? Colors.black87,
            fontWeight: FontWeight.bold,
            fontSize: 18),
        value: widget.value.toString(),
        items: widget.list!.map((valueItem) {
          return DropdownMenuItem(
              value: valueItem,
              child: Text(
                valueItem.toString(),
              ));
        }).toList(),
        onChanged: widget.onChange ??
            (val) {
              setState(() {
                widget.value = val;
              });
            },
      ),
    );
  }
}
