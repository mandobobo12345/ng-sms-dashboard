import 'package:flutter/material.dart';

import 'text_component.dart';

buildRowTable(BuildContext context, List<String> cell,
        {String? contentMessage}) =>
    TableRow(
        children: cell
            .map((e) => Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * .02),
                  child: Center(
                    child: TextComponent(
                        text: e.toString(),
                        fontWeight: FontWeight.normal,
                        color: Colors.black,
                        fontSize: 13),
                  ),
                ))
            .toList());

Padding tableText(
  BuildContext context, {
  required String text,
  required int index,
  required String alertmessage,
}) {
  return Padding(
      padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * .02),
      child: Center(
        child: TextComponent(
            text: text,
            fontWeight: FontWeight.normal,
            color: Colors.black,
            fontSize: 15),
      ));
}
