import 'package:flutter/material.dart';

import 'on_hover_component.dart';
import 'text_component.dart';

class ButtonComponent extends StatelessWidget {
  final void Function()? onPressed;

  final String? text;
  final Widget? child;
  final double? elevation;
  final double? width;
  final double? padding;
  final double? height;
  final Color? backGroundColor;
  final Color? textColor;

  const ButtonComponent({
    Key? key,
    required this.onPressed,
    this.child,
    this.backGroundColor,
    this.elevation,
    this.text,
    this.padding,
    this.height,
    this.width,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;
    return OnHoverComponent(
        child: SizedBox(
      width: width,
      height: height ?? media.height * .04,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
            backgroundColor: backGroundColor ?? Colors.deepPurple,
            padding:
                EdgeInsets.symmetric(horizontal: padding ?? media.width * .07),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
        child: child ??
            TextComponent(
                text: text!, fontSize: 15.0, color: textColor ?? Colors.white),
      ),
    ));
  }
}
