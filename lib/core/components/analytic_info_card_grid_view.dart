import 'package:flutter/material.dart';

import '../../models/model.dart';
import 'analytic_info_card.dart';

class AnalyticInfoCardGridView extends StatelessWidget {
  AnalyticInfoCardGridView({
    Key? key,
    this.crossAxisCount = 4,
    this.childAspectRatio = 1.4,
  }) : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;

  final List analyticData = [
    AnalyticInfo(
        title: "Groups and Numbers",
        count: 720,
        svgSrc: "assets/icons/Subscribers.svg",
        color: Colors.blue),
    AnalyticInfo(
        title: "Messags",
        count: 920,
        svgSrc: "assets/icons/Comments.svg",
        color: Colors.green),
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: analyticData.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: crossAxisCount,
        crossAxisSpacing: 15.0,
        mainAxisSpacing: 15.0,
        childAspectRatio: childAspectRatio,
      ),
      itemBuilder: (context, index) => AnalyticInfoCard(
        info: analyticData[index],
      ),
    );
  }
}
