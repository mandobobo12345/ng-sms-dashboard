 import 'package:flutter/material.dart';

class TextComponent extends StatelessWidget {
  final String text;
  final double? fontSize;
  final Color? color;
  final int? maxLines;
  final FontWeight? fontWeight;

  const TextComponent({
    Key? key,
    required this.text,
    this.fontSize,
    this.color,
    this.maxLines,
    this.fontWeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines ?? 1,
      textAlign: TextAlign.start,
      style: TextStyle(
          color: color ?? Colors.white,
          fontSize: fontSize ?? 30,
          fontWeight: fontWeight ?? FontWeight.bold),
    );
  }
}
