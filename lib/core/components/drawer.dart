import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:new_ng_sms/core/base/responsive.dart';
import 'package:new_ng_sms/pages/screens/home_screen.dart';
import 'package:new_ng_sms/pages/screens/login_screen.dart';

 import '../../pages/screens/send_screen.dart';
import '../../pages/screens/status_message_screen.dart';
import 'on_hover_component.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;
    return Drawer(
      backgroundColor: Colors.white,
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsetsDirectional.only(
                start: media.width * .015,
                top: media.height * .05,
                bottom: media.height * .04),
            child: Text(
              "SMS Text Message",
              style: TextStyle(
                  fontSize: media.width * .012, fontWeight: FontWeight.w500),
            ),
          ),
          Divider(color: Colors.grey.shade400, height: 3),
          remainingBalanceWidget(media, context),
          Divider(color: Colors.grey.shade400, height: 3),
          drawerListTile(
              context: context,
              title: 'Home',
              svgSrc: 'assets/icons/Dashboard.svg',
              tap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HomeScreen(),
                  ),
                );
              }),
          SizedBox(
            height: media.height * .01,
          ),
          drawerListTile(
            context: context,
            title: 'send',
            svgSrc: 'assets/icons/BlogPost.svg',
            tap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      const SendScreen(numbers: 200, balacne: "2200"),
                ),
              );
            },
          ),
          SizedBox(
            height: media.height * .01,
          ),
          drawerListTile(
              context: context,
              title: 'Messages Status',
              svgSrc: 'assets/icons/Message.svg',
              tap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const StatusMessageScreen(),
                    ));
                // NgUi.goTo('/sendstatus');
              }),
          SizedBox(
            height: media.height * .01,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: media.width * .03),
            child: const Divider(
              thickness: 0.2,
            ),
          ),
          drawerListTile(
              context: context,
              title: 'Setting',
              svgSrc: 'assets/icons/Setting.svg',
              tap: () {
                // NgLang.checkUserLang(forceAskUser: true);
              }),
          SizedBox(
            height: media.height * .01,
          ),
          drawerListTile(
              context: context,
              title: 'Language',
              iconData: Icons.language,
              tap: () {
                // NgLang.checkUserLang(forceAskUser: true);
              }),
          SizedBox(
            height: media.height * .01,
          ),
          drawerListTile(
              context: context,
              title: 'Logout',
              svgSrc: 'assets/icons/Logout.svg',
              tap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const LoginScreen(),
                    ));
              }),
        ],
      ),
    );
  }

  Container remainingBalanceWidget(Size media, BuildContext context) {
    return Container(
      padding:
          const EdgeInsetsDirectional.symmetric(horizontal: 30, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Remaining Balance",
            style: TextStyle(
                fontSize: media.width * .012, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: MediaQuery.sizeOf(context).height * .03,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                "\$26.10",
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "Add Funds",
                  style: TextStyle(
                      fontSize: media.width * .012,
                      fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  drawerListTile({
    required String title,
    String? svgSrc,
    required BuildContext context,
    Widget? leading,
    IconData? iconData,
    required Null Function() tap,
  }) =>
      OnHoverComponent(
        builder: (isHoverd) {
          final color = isHoverd ? Colors.black : Colors.grey;
          return ListTile(
            onTap: tap,
            horizontalTitleGap: 0.0,
            leading: svgSrc != null
                ? SvgPicture.asset(
                    svgSrc,
                    height: Responsive.isDesktop(context)
                        ? MediaQuery.of(context).size.width * .015
                        : MediaQuery.of(context).size.width * .05,
                    // ignore: deprecated_member_use
                    color: color,
                  )
                : Icon(iconData, color: color),
            title: Text(
              title,
              style: TextStyle(
                color: color,
              ),
            ),
          );
        },
      );
}
