import 'package:flutter/material.dart';

themeColor({required Widget child, required BuildContext context}) => Theme(
    data: Theme.of(context).copyWith(
      unselectedWidgetColor: Colors.indigo.shade500,
    ),
    child: child);
