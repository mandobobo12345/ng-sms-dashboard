import 'package:flutter/material.dart';

customAppBar({
  bool showDrawerIcon = false,
}) =>
    AppBar(
      backgroundColor: Colors.white,
      elevation: 10.0,
      leading: Center(
        child: showDrawerIcon == true
            ? Image.asset(
                "assets/message.png",
                height: 30,
                width: 30,
              )
            : const Icon(
                Icons.menu_rounded,
                color: Colors.black,
              ),
      ),
      title: const Row(
        children: [
          Text(
            "SMS Text Messaging",
            style: TextStyle(
              color: Colors.black54,
            ),
          )
        ],
      ),
      actions: [
        Center(
          child: Padding(
            padding: const EdgeInsetsDirectional.only(end: 30),
            child: Text("by Trinavo",
                style: TextStyle(color: Colors.grey.shade600)),
          ),
        ),
      ],
    );
