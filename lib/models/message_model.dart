import 'package:flutter/material.dart';

class DataMessage {
  String? dateSend;
  String? messageContent;
  String? revenue;
  List<String>? recipient;
  bool? isSend;
  bool select = false;

  DataMessage(
      {this.dateSend,
      this.revenue,
      this.messageContent,
      this.recipient,
      this.isSend});
}

class AnalyticInfo {
  final String? svgSrc, title;
  final int? count;
  final Color? color;

  AnalyticInfo({
    this.svgSrc,
    this.title,
    this.count,
    this.color,
  });
}
