import 'package:flutter/material.dart';

import '../../core/base/responsive.dart';
import '../../core/components/custom_appbar.dart';
import '../../core/components/drawer.dart';
import '../../core/components/drop_list.dart';
import '../../core/components/icon_drawer.dart';
import '../../core/components/text_component.dart';
import '../../core/components/text_field_component.dart';
import '../../core/functions/media_query.dart';

class SendScreen extends StatefulWidget {
  final String? balacne;
  final int? numbers;

  const SendScreen({required this.balacne, required this.numbers, super.key});

  @override
  State<SendScreen> createState() => SendScreenState();
}

class SendScreenState extends State<SendScreen> {
  int? totalCost = 0;

  static List<String> namesList = ["Mohamed", "Mohamed Ahmed", "Mohamed Salah"];
  dynamic valueController = namesList[0];

  TextEditingController messageContentController = TextEditingController();

  int length = 65;

  int? letters = 0;
  int numbersOfMessage = 0;
  int lastLength = 0;

  GlobalKey<ScaffoldState> key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;

    return Scaffold(
      key: key,
      appBar: customAppBar(
        showDrawerIcon: MediaQuery.sizeOf(context).width < 850 ? false : true,
      ),
      drawer: const DrawerMenu(),
      body: Row(
        children: [
          if (Responsive.isDesktop(context))
            const Expanded(child: DrawerMenu()),
          Expanded(
            flex: 4,
            child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: Media.width(context, space: .08),
                ),
                child: ListView(
                  children: [
                    IconDrawer(keyDrawer: key),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const TextComponent(
                                  text: "First Step. Read Content Message",
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                ),
                                SizedBox(
                                  height: media.height * .03,
                                ),
                                const TextComponent(
                                  color: Colors.black,
                                  text:
                                      "Second step. Select Who receives message",
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                ),
                                SizedBox(
                                  height: media.height * .03,
                                ),
                                const TextComponent(
                                  color: Colors.black,
                                  text: "Final Step. Press on Send Button",
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal,
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextComponent(
                                text: "Balance: ${widget.balacne}",
                                fontSize: 15.0,
                                color: Colors.black,
                                fontWeight: FontWeight.normal,
                              ),
                              Divider(
                                color: Colors.white,
                                height: media.height * .02,
                                endIndent: media.width * .8,
                              ),
                              TextComponent(
                                text: "Total Cost : $totalCost",
                                fontSize: 15.0,
                                color: Colors.black,
                                fontWeight: FontWeight.normal,
                              ),
                              Divider(
                                color: Colors.white,
                                height: media.height * .02,
                                endIndent: media.width * .7,
                              ),
                              TextComponent(
                                color: Colors.black,
                                text: "Numbers Count : ${widget.numbers}",
                                fontSize: 15.0,
                                fontWeight: FontWeight.normal,
                              ),
                            ],
                          ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: media.height * .03,
                    ),
                    SizedBox(
                      height: media.height * .02,
                    ),
                    Container(
                      width: media.width,
                      height: media.height * .6,
                      padding: EdgeInsets.symmetric(
                          horizontal: media.width * .03,
                          vertical: media.height * .05),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: const Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.circular(5)),
                      child: Column(
                        children: [
                          Expanded(
                              child: Row(
                            children: [
                              const TextComponent(
                                text: "Sender Name : ",
                                fontSize: 15.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                width: media.width * .07,
                              ),
                              Expanded(
                                child: DropListWidget(
                                  height: Responsive.isTablet(context) == true
                                      ? media.height * .03
                                      : media.height * .045,
                                  list: namesList,
                                  value: valueController,
                                ),
                              ),
                            ],
                          )),
                          SizedBox(
                            height: media.height * .03,
                          ),
                          Responsive.isDesktop(context) == true
                              ? Expanded(
                                  flex: 4,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      const TextComponent(
                                        text: "Message Content : ",
                                        color: Colors.black,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      SizedBox(
                                        width: media.width * .05,
                                      ),
                                      Expanded(
                                        child: TextFieldComponent(
                                          onChange: (value) {
                                            setState(() {
                                              letters = value!.length;
                                            });

                                            if (value!.length == 1) {
                                              setState(() {
                                                numbersOfMessage++;
                                              });
                                            }

                                            if (value.length == length) {
                                              setState(() {
                                                lastLength = length;
                                                length += 64;

                                                numbersOfMessage++;
                                              });
                                            }

                                            if (value.length ==
                                                lastLength - 1) {
                                              setState(() {
                                                length -= 64;
                                                numbersOfMessage--;
                                                lastLength -= 64;
                                              });
                                            }
                                            if (value.isEmpty) {
                                              setState(() {
                                                numbersOfMessage = 0;
                                                length = 64;
                                                lastLength = 0;
                                              });
                                            }
                                            return null;
                                          },
                                          maxLines: 9,
                                          width: media.width * .05,
                                          labelText: "Start Type .. ",
                                          controller: messageContentController,
                                          enabledBorderCurve: 10.0,
                                          styleBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              borderSide: const BorderSide(
                                                  color: Colors.white)),
                                        ),
                                      )
                                    ],
                                  ))
                              : Expanded(
                                  flex: 4,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const TextComponent(
                                        text: "Message Content : ",
                                        color: Colors.black,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      SizedBox(
                                        height: media.height * .02,
                                      ),
                                      TextFieldComponent(
                                        onChange: (value) {
                                          setState(() {
                                            letters = value!.length;
                                          });

                                          if (value!.length == 1) {
                                            setState(() {
                                              numbersOfMessage++;
                                            });
                                          }

                                          if (value.length == length) {
                                            setState(() {
                                              lastLength = length;
                                              length += 64;

                                              numbersOfMessage++;
                                            });
                                          }

                                          if (value.length == lastLength - 1) {
                                            setState(() {
                                              length -= 64;
                                              numbersOfMessage--;
                                              lastLength -= 64;
                                            });
                                          }
                                          if (value.isEmpty) {
                                            setState(() {
                                              numbersOfMessage = 0;
                                              length = 64;
                                              lastLength = 0;
                                            });
                                          }
                                          return null;
                                        },
                                        maxLines: 7,
                                        labelText: "Start Type .. ",
                                        controller: messageContentController,
                                        enabledBorderCurve: 10.0,
                                        styleBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            borderSide: const BorderSide(
                                                color: Colors.black)),
                                      ),
                                    ],
                                  ),
                                ),
                          Expanded(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextComponent(
                                text: "Letters : $letters ",
                                fontSize: 14.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                              TextComponent(
                                color: Colors.black,
                                text: "number of messages : $numbersOfMessage ",
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                              ),
                              ElevatedButton(
                                  onPressed: () {},
                                  child: const Icon(
                                    Icons.send_rounded,
                                    color: Colors.white,
                                  ))
                            ],
                          )),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
